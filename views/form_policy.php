<?

// вывод списка
function html_select($name, $items, $value)
{
	$html = "<select name='$name' id='$name'>";

	foreach ($items as $item)
	{
		$selected = $item == $value ? 'selected' : '';

		$html .= "<option value = '$item' $selected>$item</option>";
	}
	$html .= '</select>';
	return $html;
}

function html_form_fields($arFields, $values = [], $prefix = '')
{
	foreach ($arFields as $field)
	{
		$name = $prefix . $field['n'];
		?>
		<label><?= $field['l'] ?></label>
		<?
		if (isset($field['v']) && is_array($field['v']))
		{
			html_select($name, $field['v'], $values[$name]);
		} else
		{
			?>
			<input type='text' name='<?= $name ?>' value='<?= $values[$name] ?>'>
			<?
		}
	}
}

$arFields = [
		['n' => 'phone', 'l' => 'Телефон'],
		['n' => 'email', 'l' => 'Email'],
		['n' => 'surname', 'l' => 'Фамилия'],
		['n' => 'name', 'l' => 'Имя'],
		['n' => 'patronymic', 'l' => 'Отчество'],
		['n' => 'birthday', 'l' => 'Дата рождения'],
		['n' => 'gender', 'l' => 'Пол', 'list' => ['Мужской', 'Женский']],
		['n' => 'doc_type', 'l' => 'Тип документа'],
		['n' => 'doc_series', 'l' => 'Серия документа'],
		['n' => 'doc_number', 'l' => 'Номер документа'],
		['n' => 'doc_issued_by', 'l' => 'Кем выдан'],
		['n' => 'doc_date', 'l' => 'Когда выдан'],
		['n' => 'reg_city', 'l' => 'Город'],
		['n' => 'reg_street', 'l' => 'Улица'],
		['n' => 'reg_home', 'l' => 'Дом'],
		['n' => 'reg_housing', 'l' => 'Корпус'],
		['n' => 'reg_apartment', 'l' => 'Квартира'],
];
?>
<div id='policy_from_wrap'>
	<form name="form_policy" id="<?= $product_name ?>">
		<div id="tabs">
			<ul>
				<li><a href='#content_calc_data'>Данные расчета</a></li>
				<li><a href='#content_calc_pre'>Предрасчет</a></li>
				<li><a href='#content_insured'>Страхователь</a></li>
				<li><a href='#content_assured'>Застрахованный</a></li>
				<li><a href='#content_calc_post'>Пострасчет</a></li>
			</ul>
			<div id='content_calc_data' class='tab-content'>
				<h2>Данные расчета</h2>
				<label>Возраст</label><input type="text" name="insured_age" id="insured_age" value='<?= $inputs['insured_age'] ?>'/>
				<label>Дата действия</label><input type="text" name="date_policy_begin" class='datepicker from-now' id="date_policy_begin" value='<?= $inputs['date_policy_begin'] ?>'/>
				<input type="hidden" name="date_policy_end" class='datepicker' id="date_policy_end" value='<?= $inputs['date_policy_end'] ?>'/>
				<input type="hidden" name="kind_sport" id="kind_sport" value='ucDictiNSSportDance'/>
				<label>Номер спортсмена</label><input type="text" name="sportsman_id" id="sportsman_id" value='<?= $inputs['sportsman_id'] ?>'/>
				<label>Страховая премия</label>
				<?= html_select('insured_amount', ['500000', '900000'], $inputs['insured_amount']) ?><span> р.</span>
				<div class='bottom-panel'>
					<div id='process_calc_pre' class='tabs-next'>Расcчитать</div>
				</div>
			</div>
			<div id='content_calc_pre' class='tab-content'>
				<h2>Предварительный расчет стоимости полиса</h2>
				<div>Стоимость полиса:</div>
				<div id='calc_sum_pre'><b>0</b> р.</div>
				<div class='bottom-panel'>
					<div class='tabs-prev'>Назад</div>
					<div class='tabs-next'>Далее</div>
				</div>
			</div>
			<div id='content_insured' class='tab-content'>
				<h2>Данные страхователя</h2>

				<? html_form_fields($arFields, $inputs, 'insurer_') ?>
				<div class='bottom-panel'>
					<div class='tabs-prev'>Назад</div>
					<div class='tabs-next'>Далее</div>
				</div>
			</div>
			<div id='content_assured' class='tab-content'>
				<h2>Данные застрахованного</h2>
				<? html_form_fields($arFields, $inputs, 'insurant_') ?>
				<div class='bottom-panel'>
					<div class='tabs-prev'>Назад</div>
					<div id='process_calc_post' class='tabs-next'>Расcчитать</div>
				</div>
			</div>
			<div id='content_calc_post' class='tab-content'>
				<h2>Итоговая стоимость полиса</h2>
				<div>Стоимость полиса:</div>
				<div id='calc_sum_post'><b>0</b> р.</div>
				<div class='bottom-panel'>
					<div class='tabs-prev'>Назад</div>
					<div id='policy_buy'>Купить полис</div>
				</div>
			</div>
		</div>
	</form>
</div>