<?php

/**
 * Компонент принимающий запросы от ajax для взаимодействия с методами api_hermes
 * вызов экшенав производтся через url, посредством роутинга в классе \Hermes\Router
 * экшена по умолчанию нет
 *
 * @author orelpv
 */

namespace Hermes;

use \Hermes\Rest;
use \Hermes\Models\RestVernaModel as Model;

class RestVerna extends Rest
{
	private $model;
	private $back_url;

	public function __construct()
	{
		$this->model = new Model();
		$this->token = 'wbpFh1uxBW6TbxKO4g8ANIA7Ri1S3m';
		$this->back_url = $_SERVER['HTTP_HOST']; // ссылка возврата от постоплаты ВЕРНА
	}

	/**
	 * Получет экспресскотировку
	 *
	 */
	public function actionGetExpressQuote()
	{
		$url = '/v1/partners/dancer/quotes';

		$result = $this->sendSoapRequest($url, $_GET, 'GET');
		echo json_encode($result);

		return;
	}

	/**
	 * Создает полную котировку
	 *
	 */
	public function actionAddFullQuote()
	{
		$url = '/v1/partners/dancer/quotes';

		$result = $this->sendSoapRequest($url, $_POST, 'POST');
		echo json_encode($result);

		return;
	}

	/**
	 * Изменяет полную котировку
	 *
	 */
	public function actionUpdateFullQuote()
	{
		$url = '/v1/partners/dancer/quotes';

		$result = $this->sendSoapRequest($url, $_POST, 'PUT');
		echo json_encode($result);

		return;
	}

	/**
	 * Реализует сохранение выпущенного полиса в свою БД
	 * Данный экшен вызывается rest-сервисом callback событием после выпуска полиса в сервисе
	 *
	 * post_data:
	 * - str calc_id
	 * - str policy_id
	 *
	 * @return boolean успешность записи
	 */
	public function actionAddPolicy()
	{
		if (!$_POST['calc_id'] || !$_POST['policy_id'])
		{
			return;
		}
		$url = '/v1/partners/dancer/policies/status/' . $_POST['calc_id'];
		$result = $this->sendSoapRequest($url);
		dd($result, 1);

		if ($result['status'] === 'success')
		{
			$result = $this->model->addPolicyToDB($_POST);
		}
		return $result;
	}

	/**
	 * Редирект на генерацию формы эквайринга
	 *
	 * - post данные с формы
	 *
	 * @return boolean успешность записи
	 */
	public function actionRedirectToFormBuy()
	{
		$url = "/v1/payments/router/run/get-form";
		$data = [
			'calc_id' => $_GET['calc_id'],
			'back_url' => $this->back_url,
		];
		$result = $this->sendSoapRequest($url, $data, 'GET', ['data_type' => 'html']);
		echo $result;

		return;
	}

	public function actionGoPolicy()
	{
		$data_soap['calc_id'] = '6985-KO5JTVU';
		$result = $this->sendSoapRequest('/v1/partners/dancer/policies', $data_soap, 'POST');
		//$result = $this->model->addPolicyToDB($_POST);
		dd($result, 1);
		return $result;
	}

}
