<?php

include_once "system/router.php";

try
{
	$component = new \Hermes\Router();
	$result = $component->runComponent();
} catch (Exception $e)
{
	exAnswer($e);
}
