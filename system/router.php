<?php

namespace Hermes;

/**
 * Класс для распределения вызова от component-сети до рабочего класса
 *
 * @author OrelPV
 */
class Router
{
	public $component;

	public function __construct($name_component = false)
	{
		include_once 'system/bootstrap.php';
		$url = $_SERVER['REQUEST_URI'];
		$s = preg_match('/^\/(.+?)\/([a-z-]+)\/?/i', $url, $matches);
		$name_component = $matches[1];
		$name_action = $matches[2];

		$this->component = $this->identifyComponent($name_component, $name_action);
	}

	// перенаправление на компонент
	protected function identifyComponent($name_component, $name_action)
	{
		if (!$name_component || !$name_action)
		{
			bugproc("routerComponent: Не указан компонент илил имя экшена");
		}
		$path = 'components/' . str_replace('-', '_', $name_component) . ".php";
		$this->action_name = str_replace('-', '', ucwords($name_action, '-'));

		if (!file_exists($path))
		{
			bugproc("routerComponent: Файл не найден ($path)");
		}
		include_once $path;
		$class_name = '\\Hermes\\' . str_replace('-', '', ucwords($name_component, '-'));
		$obj_class = new $class_name();

		return $obj_class;
	}

	public function runComponent($params = [])
	{
		$action = 'action' . $this->action_name;

		if (is_object($this->component) && method_exists($this->component, $action))
		{
			return call_user_func([$this->component, $action], $params);
		} else
		{
			bugproc("routerComponent: Метод $action отсутствует");
		}
	}

}
