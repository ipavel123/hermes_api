<?php

/**
 * Модель для работы с компонентом \Hermes\RestVerna
 *
 * @author orelpv
 */

namespace Hermes\Models;

use \Hermes\Models\BaseModel;

class RestVernaModel extends BaseModel
{

	/**
	 * Метод добавления выпущенного полиса в свою БД
	 * реализация метода разработчика-партера
	 *
	 * @param type $data
	 */
	public function addPolicyToTable($data = [])
	{

		$filename = dirname(__FILE__) . '/atol-log-post.txt';
		if (!empty($_POST))
		{
			$dh = fopen($filename, 'a+');
			fwrite($dh, "--- POST ----");
			fwrite($dh, var_export($_POST, true));
			fclose($dh);
		}

		$filename = dirname(__FILE__) . '/atol-log-post.txt';
		if (!empty($_GET))
		{
			$dh = fopen($filename, 'a+');
			fwrite($dh, "--- GET " . date('d.m.Y H:i:s') . "----");
			fwrite($dh, var_export($_GET, true));
			fclose($dh);
		}
		return true;
	}

}
