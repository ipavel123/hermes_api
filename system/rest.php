<?php

/*
 * Класс для работы с restfull сервисом от СО ВЕРНА
 *
 * исходный код на git:
 *
 * @author developer_verna
 */

namespace Hermes;

class Rest
{
	protected $token;

	const HOST = 'http://rest-hermes.ru';

	public function __construct($token)
	{
		$this->token = $token;
	}

	/**
	 * Создает rest клиент и отправляет данные на указанный url
	 *
	 * @param string $url - ссылка на ресурс rest-сервиса
	 * @param type $data - передаваемые в поток данные
	 * @param type $method - тип метода (GET,POST,PUT)
	 * @param type $attr - доп. атрибуты
	 * @return array
	 */
	protected function sendSoapRequest($url, $data = [], $method = 'GET', $attr = [])
	{
		$ch = curl_init();
		$url = self::HOST . $url;

		$_attr['headers'] = ['Authorization: Bearer ' . $this->token];
		$_attr['data_type'] = 'json';
		$attr = array_merge($_attr, $attr);

		if ($method == 'GET')
		{
			$params = http_build_query($data);
			$url .= '?' . ($params ?: '');
		}
		if (in_array($method, ['PUT', 'POST']))
		{
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
			// CURLOPT_POSTFIELDS подмассивы не передаёт, поэтому преобразоваваем в формат "параметров запроса"
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		}
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $attr['headers']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);

		curl_close($ch);

		if ($attr['data_type'] == 'json')
		{
			$output = json_decode($output, true); // вывод json в виде массива
			$output = $output['result'] ?: $output;
		}
		if ($attr['data_type'] == 'html')
		{
			$output = html_entity_decode($output);
		}
		return $output;
	}

}
