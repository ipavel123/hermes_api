<?
header('Content-Type: text/html; charset=UTF-8');
ini_set('mbstring.internal_encoding', 'UTF-8');
?>
<html>
	<?php
	include_once 'system/template.php';

	$api = new Hermes\Template();

	$form = $api->renderPolicyForm(['inputs' => $_GET]);
	?>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type="text/css" href="views/css/jquery-ui.min.css" rel="stylesheet">
		<link type="text/css" href="views/css/jquery-ui.theme.min.css" rel="stylesheet">
		<link type="text/css" href="views/css/style.css" rel="stylesheet">
		<script type="text/javascript" src="views/js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="views/js/jquery-ui.min.js"></script>

	</head>
	<body>
		<?= $form ?>
		<script type="text/javascript" src="views/js/script.js"></script>
	</body>
</html>
