<?php

// если режим отладки, то выбрасываем ошибку, если нет то просто возврат
function bugproc($msg, $code = 1)
{
	$code = $_REQUEST['dev_mode'] ?: $code;
	ob_start();
	debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
	$debug = ob_get_contents();
	ob_end_clean();

	$sep = '<br/>' . str_pad('', 30, '-') . '<br/>';
	$msg = '<pre>' . $sep . $msg . $sep . $debug . $sep . '</pre>';
	throw new Exception($msg, +$code); // код 1 по умлчанию выводит сообщение
}

// вывод сообщения исколючения и ответ на код ошибки
function exAnswer(Exception $e)
{
	$code = $e->getCode();
	$msg = $e->getMessage();

	// код 1 выводим сообщение в режиме dev
	if ($code === 1)
	{
		echo($msg);
	}
}

function dd($out, $is_die = false)
{
	echo "<pre class='debag-dd'>";
	var_dump($out);
	echo '</pre>';

	if ($is_die)
	{
		die();
	}
}

// xml объект перебразует в массив
function json_array(object $data)
{
	$result = json_decode(json_encode($data, JSON_UNESCAPED_UNICODE), 1);
}
