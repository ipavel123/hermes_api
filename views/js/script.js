var hermes = hermes || {};

var main_wrap = $('#policy_from_wrap');
var form = main_wrap.find('form');

// пресеты запросов
var url_presets = {
	calc_sum: {
		url: '/rest-verna/get-express-quote',
		method: 'GET'
	},
	add_quote: {
		url: '/rest-verna/add-full-quote',
		method: 'POST'
	},
	update_quote: {
		url: '/rest-verna/update-full-quote',
		method: 'PUT'
	},
	form_buy: {
		url: '/rest-verna/redirect-to-form-buy',
		method: 'GET'
	},
};

// предыдущая вкладка
hermes.tabs_button_prev = function (e)
{
	var prev_id = $(e.target).closest(".tab-content").prev().index();
	var active = prev_id - 1;
	$('#tabs', main_wrap).tabs("disable")
			.tabs("enable", active)
			.tabs("option", "active", active);
}
// следующая вкладка
hermes.tabs_button_next = function (e)
{
	var next_id = $(e.target).closest(".tab-content").next().index();
	var active = next_id - 1;
	$('#tabs', main_wrap).tabs("disable")
			.tabs("enable", active)
			.tabs("option", "active", active);
}
// отправляет запрос ajax из установленных персетов url_presets исользуя callback функцию по завершению
hermes.send_form_json = function (url_name, data, custom_callback)
{
	var preset = url_presets[url_name];
	hermes.loading_start();

	$.ajax(
			{
				url: preset.url,
				method: preset.method,
				data: data,
				dataType: 'json',
				success: function (data)
				{
					var verify = hermes.output_error(data);
					hermes.loading_end();
					custom_callback(verify, data); // вначале передаем результат проверки, а потом результат запроса ajax
				}
			});
	return;
}
// вывод ошибки возврата ajax запроса
hermes.output_error = function (data)
{
	var verify = true;

	if (data && data.hasOwnProperty('error'))
	{
		var text_error = '';

		var arError = data.error['row'];

		if (!Array.isArray(arError))
		{
			arError = [arError];
		}
		$(arError).each(function (i, error_val)
		{
			text_error += '---\n' + error_val.attr_name + ': ' + error_val.text + '\n';
		});
		alert(text_error);
		verify = false;
	}
	return verify;
}
// возвращаемся на предыдущий таб
hermes.tab_active_back = function ()
{
	var tab_obj = main_wrap.find('.tab-content:visible').find('.tabs-prev');
	tab_obj.trigger('click');
}
// отображение процесса загрузки
hermes.loading_start = function ()
{
	var tags = $("<div id='verna-loading-background'>")

	tags.append("<div id='verna-loading-squad'>");

	$('body').append(tags);
}
// скрытие процесса загрузки
hermes.loading_end = function ()
{
	$('body').find('#verna-loading-background').remove();
}

//----- полисы -----

// вызов и обработка запроса на экспресс котировку
hermes.process_calc_pre = function (e)
{
	var insured = {};

	$('#content_calc_data', form).find("[name^=insured_]").each(function ()
	{
		insured[$(this).attr('name')] = $(this).val();
	}
	);
	var result = hermes.send_form_json('calc_sum', insured, function (verify, data)
	{
		if (verify)
		{
			$('#calc_sum_pre b', form).text(data.calc_sum);
		} else
		{
			hermes.tab_active_back();
		}
	});
}
// вызов и обработка запроса на создание полной котировки
hermes.process_calc_post = function (e)
{
	var preset;

	if ($('#calc_id', form).length == 0 || $('#calc_id', form).val() == '')
	{
		preset = 'add_quote';
	} else
	{
		preset = 'update_quote';
	}
	var result = hermes.send_form_json(preset, form.serializeArray(), function (verify, data)
	{
		if (verify)
		{
			$('#calc_sum_post b', form).text(data.calc_sum);

			if ($('#calc_id', form).length == 0)
			{
				form.append("<input type='hidden' name='calc_id' id='calc_id'>");
			}
			if ($('#calc_sum', form).length == 0)
			{
				form.append("<input type='hidden' name='calc_sum' id='calc_sum'>");
			}
			$('#calc_id', form).val(data.calc_id);
			$('#calc_sum', form).val(data.calc_sum);
		} else
		{
			hermes.tab_active_back();
		}
	});
}
// покупка полиса
hermes.policy_buy = function (e)
{
	var calc_id = $('#calc_id', form);

	if (calc_id.length != 0 && calc_id.val() != '')
	{
		location.href = url_presets.form_buy['url'] + '?calc_id=' + calc_id.val();
	}
}

$(function ()
{
	$('.tabs-prev', main_wrap).on('click', hermes.tabs_button_prev);
	$('.tabs-next', main_wrap).on('click', hermes.tabs_button_next);
	$('#process_calc_pre', main_wrap).on('click', hermes.process_calc_pre);
	$('#process_calc_post', main_wrap).on('click', hermes.process_calc_post);
	$('#policy_buy', main_wrap).on('click', hermes.policy_buy);
	$('#tabs', main_wrap).tabs({disabled: true}).tabs("enable", 0);
	$('.datepicker', main_wrap).datepicker({dateFormat: 'dd-mm-yy'});
	$('.datepicker.from-now', main_wrap).datepicker("option", "minDate", '+1d');
	$('#date_policy_begin', main_wrap).change(function ()
	{
		var d_begin = $(this);
		var d_end = $('#date_policy_end', main_wrap);
		var ddd = new Date(d_begin.datepicker('getDate')).getTime() + 364 * 24 * 3600 * 1000; // плюсуем год вперед к установленной дате
		d_end.datepicker('setDate', new Date(ddd));
	});
});
