<?php

$ar_get = [
	'insured_age' => '35',
	'insured_amount' => '900000',
	'insurer_phone' => '+7-988-445-65-12',
	'sportsman_id' => '112245',
	'insurer_email' => '1@1.ru',
	'insurer_surname' => 'Сердинко',
	'insurer_name' => 'Костя',
	'insurer_patronymic' => 'Артурович',
	'insurer_birthday' => '15.04.1987',
	'insurer_gender' => 'Мужской',
	'insurer_doc_type' => 'Паспорт РФ',
	'insurer_doc_series' => '1254',
	'insurer_doc_number' => '12354',
	'insurer_doc_issued_by' => 'Кукуево',
	'insurer_doc_date' => '16.03.2018',
	'insurer_reg_city' => 'Кукуево',
	'insurer_reg_street' => 'Кукуево-зуево',
	'insurer_reg_home' => '145',
	'insurer_reg_housing' => '5',
	'insurer_reg_apartment' => '45',
	'insurant_phone' => '79884456512',
	'insurant_email' => '1@1.ru',
	'insurant_surname' => 'Сердинко',
	'insurant_name' => 'Костя',
	'insurant_patronymic' => 'Артурович',
	'insurant_birthday' => '15.04.1987',
	'insurant_gender' => 'Мужской',
	'insurant_doc_type' => 'Паспорт РФ',
	'insurant_doc_series' => '1254',
	'insurant_doc_number' => '12354',
	'insurant_doc_issued_by' => 'Кукуево',
	'insurant_doc_date' => '16.03.2018',
	'insurant_reg_city' => 'Кукуево',
	'insurant_reg_street' => 'Кукуево-зуево',
	'insurant_reg_home' => '145',
	'insurant_reg_housing' => '5',
	'insurant_reg_apartment' => '45',
];

$ar_get = http_build_query($ar_get);

header("Location: /test.php?$ar_get");
?>
