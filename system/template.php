<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hermes;

/**
 * Description of template
 *
 * @author orelpv
 */
class Template
{

	public function init()
	{
		return new self;
	}

	/**
	 * Получение html форму с заполненными данными полей
	 *
	 * @param array $data - данные формы
	 * @return возвращает html форму полиса
	 */
	public function renderPolicyForm($data = [])
	{
		if ($data)
		{
			$data = $this->prepare($data);
		}
		ob_start();

		extract($data);

		include_once 'views/form_policy.php';

		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	public function prepare($data)
	{
		if (is_array($data))
		{
			foreach ($data as $k => $value)
			{
				$result[$k] = $this->prepare($value);
			}
		} else
		{
			$result = htmlspecialchars($data);
		}

		return $data;
	}

}
